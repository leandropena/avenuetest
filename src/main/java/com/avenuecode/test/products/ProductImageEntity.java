package com.avenuecode.test.products;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author leandrobarreto
 * @date 11st july 2017
 * 
 * @description AvenueCodeProductImage
 * 
 */

@Entity
@Table(name="product_image")
public class ProductImageEntity extends Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	int id; //pk
	
	@Column(name="type")
	int type; //int para externalizar os tipos em um outro objeto :)

	@Column(name="product_id")
	int product_id; //parent product fk
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getProduct_id() {
		return product_id;
	}
	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
}
