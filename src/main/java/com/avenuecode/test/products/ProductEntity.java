package com.avenuecode.test.products;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author leandrobarreto
 * @date 11st july 2017
 * 
 * @description AvenueCodeProduct
 * 	
 */

@Entity
@Table(name="product")
public class ProductEntity extends Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	int id; //pk
	
	@Column(name="name")
	String name;
	
	@Column(name="description")
	String description;
	
//	@Column(name="parent_product_id")
//	Product parent_product; //yet another product id (product can be also a group of products) // FIXME 
	
	@Column(name="image_id")
	Product image; //image's fk
	
//	public Product getParent_product() {
//		return parent_product;
//	}
//	public void setParent_product_id(Product parent_product) {
//		this.parent_product = parent_product;
//	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
