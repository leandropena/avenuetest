package com.avenuecode.test.h2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.sql.DataSource;

public class DataBaseConn {
	public static DataBaseConn dataServiceHelper = null;
	private Connection con = null;
	DataSource dataSource = null;
	InitialContext initialContext = null;
	public static final String DB_URL = "jdbc:h2:tcp://localhost/~/test";
	public static final String DRIVER_NAME = "org.h2.Driver";
	/**
	 
	* This method is used to create an object for the given DAO class name.
	*/
	 
	public Connection getConnection() throws ClassNotFoundException,
	SQLException {
	Class.forName(DRIVER_NAME);
	con = DriverManager.getConnection(DB_URL, "sa", "");
	return con;
	}
	 
	public void closeConnection() throws SQLException {
	if (isConnectionOpen()) {
	con.close();
	con = null;
	}
	}
	public boolean isConnectionOpen() {
	return (con != null);
	}
	 
	public static DataBaseConn getInstance() {
	if (dataServiceHelper == null) {
	dataServiceHelper = new DataBaseConn();
	}
	return dataServiceHelper;
	}
	 // FIXME
	public void executeUpdateQuery(String query) throws SQLException,
	ClassNotFoundException {
	Connection con = getConnection();
	Statement stmt = con.createStatement();
	stmt.execute(query);
	closeConnection();
	}
	 
	public List<Object> executeQuery(String query) throws ClassNotFoundException,
	SQLException {
	Connection con = getConnection();
	Statement stmt = con.createStatement();
	ResultSet rs = stmt.executeQuery(query);
	List<Object> als = convertPojoList(rs);
	closeConnection();
	return als;
	}
	 
	private List<Object> convertPojoList(ResultSet rs) throws SQLException {
	List<Object> asl = new ArrayList<Object>();
	while (rs.next()) {
//	User user = new Product(rs.getString("name"), rs.getString("password")); //FIXME
	Object user = null;
	asl.add(user);
	}
	return asl;
	}
	 
	public static void main(String[] args) throws ClassNotFoundException,
	SQLException {
	String query = "Select * from user where name='nitin'";
	List<Object> als = DataBaseConn.getInstance().executeQuery(query);
	System.out.println("listando ---> " + als);
	}
}
