package com.avenuecode.test.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("/main")
public class MainService {
	@GET
	@Path("/{param}")
	public Response getMsg(@PathParam("param") String msg) {
		
		String output = "output: " + msg;

		return Response.status(200).entity(output).build();

	}
}
