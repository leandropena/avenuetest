package com.avenuecode.test.services;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import com.sun.jersey.multipart.FormDataParam; //torcendo os dedos aqui pra rodar!!


/**
 * 
 * @author leandrobarreto
 *
 */
@Path("/upload")
public class UploadService {
	@GET
	@Path("/{param}")
	public Response getMsg(@PathParam("param") String msg) {
		
		String output = "gravando os dados.. " + msg;

		return Response.status(200).entity(output).build();

	}
	
	@POST
	public Response postMsg(
			@FormDataParam("prod_name") String prod_name
		) {
		String str_prod_name = " 2 prod_name: " + prod_name;
		
		// TODO capturar os dados do form
		// TODO gravar e persistir as entidades
		
		return Response.status(200).entity(str_prod_name).build();
		
		
	}
	
	
	// save uploaded file to a defined location on the server
/*		private void saveFile(InputStream uploadedInputStream, String serverLocation) {

			try {
				OutputStream outpuStream = new FileOutputStream(new File(
						serverLocation));
				int read = 0;
				byte[] bytes = new byte[1024];

				outpuStream = new FileOutputStream(new File(serverLocation));
				while ((read = uploadedInputStream.read(bytes)) != -1) {
					outpuStream.write(bytes, 0, read);
				}

				outpuStream.flush();
				outpuStream.close();

				uploadedInputStream.close();
			} catch (IOException e) {

				e.printStackTrace();
			}

		}*/
		
}
